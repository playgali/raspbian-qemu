FROM balenalib/rpi-raspbian
MAINTAINER Galileo Martinez "playgali@gmail.com"
ENV REFRESHED_AT 2017-Sep-22

ENV QEMU_EXECVE 1

COPY . /usr/bin

RUN [ "qemu-arm-static", "/bin/sh", "-c", "ln /bin/sh /bin/sh.real" ]
